LSTM-based Inverse Dynamics Learning for Franka Emika Panda
===========================================================

.. important::

   This work is asscociated with the `IEEE publication LSTM-based Inverse Dynamics Learning for Franka Emika Robot by Schneider and Gorißen et al. 2024 <https://ieeexplore.ieee.org/document/10553865>`_

* Free software: MIT license
* `Supplementary Information <https://jan-niklas-schneider.pages.git-ce.rwth-aachen.de/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/>`_
* `Repository <https://git-ce.rwth-aachen.de/jan-niklas-schneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda>`_

Features
--------

* Project is based on Franka Emika Panda robot
* Motion parameter dataset acquiered with ROS and MoveIt 
* LSTM-based neural network for inverse dynamic modelling

Repository Organisation
-----------------------

.. code-block:: text

    LSTM-based_inverse_dynamics_learning_for_franka_emika_panda
    ├── data: here you can find the datasets in different versions
    │   ├── dataset_v1: first iteration
    │   ├── dataset_v2: second iteration
    │   ├── dataset_v3: third iteration
    │   └── ... 
    ├── src: here you find source code that is not directly related to data generation, preprocessing, analysis and neural network 
    │   ├── Data_Analysis: analysis scripts based on python
    │   ├── Data_Preprocessing: 
    │   ├── Inverse_Dynamics: Deep Learning source code
    │   │   ├── test results: figures and csv files 
    │   │   └── ... 
    │   ├── ROS: Data generation with ROS
    │   └── ... 
    ├── docs: everything related to the Supplementary Information pages
    └── ... 
