####
Data
####

********
Overview
********

In sum, three different datasets are recorded.
Each dataset was measured with a different frequency (see table below).
Detailed information and analysis can be found at the subpages.
All dataset are saved in csv files and can be found in the repository (see link in table).


.. list-table:: Overview Datasets
   :header-rows: 1

   - - Version
     - Measurement Frequency
     - Method
     - Files
     - Info
   - - `V1 <dataset_v1>`_
     - 50 Hz 
     - Random Pick & place in cartesian and joint space
     - `Data <https://git-ce.rwth-aachen.de/jan-niklas-schneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/-/tree/main/data/dataset_v1>`__
     - 
   - - `V2 <dataset_v2>`_
     - 30 Hz
     - Random joint configurations. 
     - `Data <https://git-ce.rwth-aachen.de/jan-niklas-schneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/-/tree/main/data/dataset_v2>`__
     - Includes voltage, current and power of robot in sum
   - - `V3 <dataset_v3>`_
     - 100 Hz
     - Random joint configurations. ISO9283 test trajectory.
     - `Data <https://git-ce.rwth-aachen.de/jan-niklas-schneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/-/tree/main/data/dataset_v3>`__
     - Dataset investigated in the paper

*******************
Directory structure
*******************

.. code-block:: text

    Dataset v#
    ├── analysis: here you can find the analysis results in csv tables as well as svg images
    │   ├── feature_distribution
    │   ├── feature_errorbar
    │   ├── frequency_analysis
    |   └── trajectory_analysis
    ├── test: here you find the test dataset
    │   ├── *_com.csv: raw command trajectory
    │   ├── *_interp_com.csv: interpolated command trajectory (respect to measured timesteps and normalized)
    │   ├── *_interp_rtbm.csv: interpolated matlab torques (respect to measured timesteps)
    │   ├── *_meas.csv: raw measured attained trajectory
    │   ├── *_rtbm.csv: Matlab torques respect to raw command trajectory
    │   └── ... 
    └── train: here you find train dataset
        ├── *_com.csv: raw command trajectory
        ├── *_interp_com.csv: interpolated command trajectory (respect to measured timesteps)
        ├── *_meas.csv: raw measured attained trajectory
        └── ... 

******************************
How trajectories are generated
******************************

Three types of trajectories are used. 
You can look them up
`here <trajectory_generation>`_.

**********************
What is saved and how?
**********************

.. list-table:: Command Trajectory (files ending with \*\_com.csv)
   :header-rows: 1

   * - Parameter
     - Unit
     - Variable name(s)
   * - Time
     - seconds 
     - t_com
   * - Joint Positions
     - rad
     - q1_com, ..., q7_com
   * - Joint Velocities
     - rad/s
     - qd1_com, ..., qd7_com
   * - Joint Accelerations
     - rad/s :sup:`2`
     - qdd1_com, ..., qdd7_com


.. list-table:: Measurements (files ending with \*\_meas.csv)
   :header-rows: 1

   * - Parameter
     - Unit
     - Variable name(s)
   * - Time
     - seconds 
     - t_meas
   * - Joint Positions
     - rad
     - q1_meas, ..., q7_meas
   * - Joint Velocities
     - rad/s
     - qd1_meas, ..., qd7_meas
   * - Joint Torques
     - Nm
     - tau1_meas, ..., tau7_meas
   * - Voltage (total) updated every 500 ms, only V2
     - Volt
     - U  
   * - Current (total) updated every 500 ms, only V2
     - Ampere
     - I 
   * - Power (total) updated every 500 ms, only V2
     - Watt
     - P



******************
Robot joint limits
******************

The robot joint limits in MoveIt are limited to 
0.1 rad less in magnitude than the hardware limits.
The robot acceleration limits in MoveIt are adjusted 
as stated in MoveIt configuration documentation:

    As MoveIt! does not support jerk limits, the acceleration limits provided here are the highest values that guarantee
    that no jerk limits will be violated. More precisely, applying Euler differentiation in the worst case (from min accel
    to max accel in 1 ms) the acceleration limits are the ones that satisfy
    max_jerk = (max_acceleration - min_acceleration) / 0.001
      
.. list-table:: MoveIt Limits
   :header-rows: 1

   * - Name
     - Joint 1
     - Joint 2
     - Joint 3
     - Joint 4
     - Joint 5
     - Joint 6
     - Joint 7
     - Unit
   * - q :sub:`max`
     - 2.7973
     - 1.6628
     - 2.7973
     - -0.1698
     - 2.7973
     - 3.7525
     - 2.7973
     - rad
   * - q :sub:`min`
     - -2.7973
     - -1.6628
     - -2.7973
     - -2.9718
     - -2.7973
     - 0.0825
     - -2.7973
     - rad
   * - qd :sub:`max`
     - 2.1750
     - 2.1750
     - 2.1750
     - 2.1750
     - 2.6100
     - 2.6100
     - 2.6100
     - rad/s
   * - qdd :sub:`max`
     - 3.75
     - 1.875
     - 2.5
     - 3.125
     - 3.75
     - 5
     - 5
     - rad/s :sup:`2`

source:
https://github.com/ros-planning/panda_moveit_config/blob/noetic-devel/config/joint_limits.yaml

(adjusted)
 

.. list-table:: Physical Limits
   :header-rows: 1

   * - Name
     - Joint 1
     - Joint 2
     - Joint 3
     - Joint 4
     - Joint 5
     - Joint 6
     - Joint 7
     - Unit
   * - q :sub:`max`
     - 2.8973
     - 1.7628
     - 2.8973
     - -0.0698
     - 2.8973
     - 3.7525
     - 2.8973
     - rad
   * - q :sub:`min`
     - -2.8973
     - -1.7628
     - -2.8973
     - -3.0718
     - -2.8973
     - -0.0175
     - -2.8973
     - rad
   * - qd :sub:`max`
     - 2.1750
     - 2.1750
     - 2.1750
     - 2.1750
     - 2.6100
     - 2.6100
     - 2.6100
     - rad/s
   * - qdd :sub:`max`
     - 15
     - 7.5
     - 10
     - 12.5
     - 15
     - 20
     - 20
     - rad/s :sup:`2`
   * - tau :sub:`max`
     - 87
     - 87
     - 87
     - 87
     - 12
     - 12
     - 12
     - Nm

source:
https://frankaemika.github.io/docs/control_parameters.html#limits-for-panda

 