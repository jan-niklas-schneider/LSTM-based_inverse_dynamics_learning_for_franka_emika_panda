##########
Dataset V2
##########

***********************
Dataset characteristics
***********************

.. list-table:: Dataset characteristics
   :header-rows: 1

   * -  
     - Training
     - Validation
     - Test
   * - # Joint space trajectories
     - 418
     - 104
     - 1

*************************
Training Dataset Analysis
*************************

.. csv-table:: Trajectory Analysis
   :file: ../../data/dataset_v2/analysis/trajectory_analysis_train.csv
   :header-rows: 1

Measurement Frequency Analysis
##############################

.. figure:: ../../data/dataset_v2/analysis/hist_freq_train.png

Command Joint Position by MoveIt
################################

.. list-table:: 
   :widths: 40 40 
   :header-rows: 1

   * - Min/Max & Std 
     - Histogram
   * - .. image:: ../../data/dataset_v2/analysis/errorbar_train_q.png
     - .. image:: ../../data/dataset_v2/analysis/hist_train_q.png

.. csv-table:: Feature Analysis
   :file: ./../../data/dataset_v2/analysis/feature_analysis_train_q.csv
   :header-rows: 1


Command Joint Velocity by MoveIt
################################

.. list-table:: Plots
   :widths:  40 40 
   :header-rows: 1

   * - Min/Max & Std 
     - Histogram
   * - .. image:: ../../data/dataset_v2/analysis/errorbar_train_qd.png
     - .. image:: ../../data/dataset_v2/analysis/hist_train_qd.png


.. csv-table:: Feature Analysis
   :file: ../../data/dataset_v2/analysis/feature_analysis_train_qd.csv
   :header-rows: 1


Command Joint Acceleration by MoveIt
####################################

Physical joint acceleration limits are marked hatched.
MoveIt joint acceleration limits are marked blue.

.. list-table:: Plots
   :widths: 40 40 
   :header-rows: 1

   * - Min/Max & Std 
     - Histogram
   * - .. image:: ../../data/dataset_v2/analysis/errorbar_train_qdd.png
     - .. image:: ../../data/dataset_v2/analysis/hist_train_qdd.png


.. csv-table:: Feature Analysis
   :file: ../../data/dataset_v2/analysis/feature_analysis_train_qdd.csv
   :header-rows: 1

Measured Joint Torques by internal sensors
##########################################

.. list-table:: Plots
   :widths: 40 40 
   :header-rows: 1

   * - Min/Max & Std 
     - Histogram
   * - .. image:: ../../data/dataset_v2/analysis/errorbar_train_tau.png
     - .. image:: ../../data/dataset_v2/analysis/hist_train_tau.png


.. csv-table:: Feature Analysis
   :file: ../../data/dataset_v2/analysis/feature_analysis_train_tau.csv
   :header-rows: 1

*********************
Test Dataset Analysis
*********************

  
Command Joint Position by MoveIt
################################

.. list-table:: 
   :widths: 40 40 
   :header-rows: 1

   * - Min/Max & Std 
     - Histogram
   * - .. image:: ../../data/dataset_v2/analysis/errorbar_test_q.png
     - .. image:: ../../data/dataset_v2/analysis/hist_test_q.png

.. csv-table:: Feature Analysis
   :file: ./../../data/dataset_v2/analysis/feature_analysis_test_q.csv
   :header-rows: 1


Command Joint Velocity by MoveIt
################################

.. list-table:: Plots
   :widths:  40 40 
   :header-rows: 1

   * - Min/Max & Std 
     - Histogram
   * - .. image:: ../../data/dataset_v2/analysis/errorbar_test_qd.png
     - .. image:: ../../data/dataset_v2/analysis/hist_test_qd.png


.. csv-table:: Feature Analysis
   :file: ../../data/dataset_v2/analysis/feature_analysis_test_qd.csv
   :header-rows: 1


Command Joint Acceleration by MoveIt
####################################

.. list-table:: Plots
   :widths: 40 40 
   :header-rows: 1

   * - Min/Max & Std 
     - Histogram
   * - .. image:: ../../data/dataset_v2/analysis/errorbar_test_qdd.png
     - .. image:: ../../data/dataset_v2/analysis/hist_test_qdd.png


.. csv-table:: Feature Analysis
   :file: ../../data/dataset_v2/analysis/feature_analysis_test_qdd.csv
   :header-rows: 1

Measured Joint Torques by internal sensors
##########################################

.. list-table:: Plots
   :widths: 40 40 
   :header-rows: 1

   * - Min/Max & Std 
     - Histogram
   * - .. image:: ../../data/dataset_v2/analysis/errorbar_test_tau.png
     - .. image:: ../../data/dataset_v2/analysis/hist_test_tau.png


.. csv-table:: Feature Analysis
   :file: ../../data/dataset_v2/analysis/feature_analysis_test_tau.csv
   :header-rows: 1
    