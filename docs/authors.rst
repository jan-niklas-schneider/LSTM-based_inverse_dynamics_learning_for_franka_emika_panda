=======
Credits
=======

Development Lead
----------------

* Jan-Niklas Schneider <jan-niklas.schneider@llt.rwth-aachen.de>

Contributors
------------

* Leon Gorißen <leon.gorissen@llt.rwth-aachen.de>
* Thomas Kaster <thomas.kaster@llt.rwth-aachen.de>
* Philipp Walderich <philipp.walderich@llt.rwth-aachen.de>
* Christian Hinke <christian.hinke@llt.rwth-aachen.de>
