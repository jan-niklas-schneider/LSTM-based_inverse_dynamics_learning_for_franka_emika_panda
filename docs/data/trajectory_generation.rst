.. _trajectory_generation:

#####################
Trajectory Generation
#####################


*********************
Random Pick and Place
*********************

    Used in dataset V1

A random pick and place task is executed.
A random start and random target position (cartesian) is used.
After start, the tcp moves up 10cm, go the target position, go 
down 10cm.
The trajectory is calculated in cartesian and joint space.
An example pick and place trajectory is depicted in the following 
image.


Pick and Place Trajectory in Task Space
=======================================

Interpolated in joint space
---------------------------

.. image:: example_trajectory_pickandplace_task_1.png


Interpolated in task space
--------------------------

.. image:: example_trajectory_pickandplace_task_2.png


Pick and Place Trajectory in Joint Space
========================================

Interpolated in joint space
---------------------------

.. image:: example_trajectory_pickandplace_joint_1.png


Interpolated in task space
--------------------------

.. image:: example_trajectory_pickandplace_joint_2.png


Video of Random Pick and Place
==============================

* First: Interpolated in cartesian space
* Second: Interpolated in joint space

.. raw:: html

    <embed>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/_fE7tPmV8Tk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </embed>



**************************
Random joint configuration 
**************************

    Used in dataset V2, V3

A random joint configuration is sampled.
The trajectory to this target is calculated started
from the actual joint position. 
This results in a trajectory as examplary depicted in the 
following picture.


Random Joint Trajectory in Task Space
=====================================

.. image:: example_trajectory_random_joint_task.png


Random Joint Trajectory in Joint Space
======================================

.. image:: example_trajectory_random_joint.png


Video of Random Joint Trajectory Generation
===========================================

.. raw:: html

    <embed>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/ZAGxRc4KSUI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </embed>


***********************
ISO9283 test trajectory
***********************

    Used in dataset V3 (test)

For evaluation, the optional test path for 400 x 400 mm plane 
in A.5 (ISO 9283 - Manipulating industrial robots - Performance criteria and related test methods,
9283:1998, International Organization for Standardization, 1998.) is used.
A velocity factor of 0.25, an acceleration factor of 0.1 is chosen.
Start position is [-0.18, 0.6, 0.2, -np.pi, 0, 0].


ISO9283 Trajectory in Task Space
================================

.. image:: example_trajectory_iso9283_task.png


ISO9283 Trajectory in Joint Space
=================================

.. image:: example_trajectory_iso9283.png