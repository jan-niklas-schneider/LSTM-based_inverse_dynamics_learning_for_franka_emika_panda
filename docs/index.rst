.. include:: ../README.rst 
  
Contents
--------
.. toctree::
   :maxdepth: 2

   data/index
   neural_network/index
   authors

Indices and tables
------------------
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


