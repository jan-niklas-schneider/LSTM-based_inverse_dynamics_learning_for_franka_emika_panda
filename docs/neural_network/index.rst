==============
Neural Network
==============

The training of the neural network is based on dataset V3. 
An optimized network architecture is derived by hyperparameter search.
The hyperparameter search is based on Weights & Biases.
The neural network predictions are conducted with a test dataset and 
are analyzed in frequency domain.

********
Contents
********

.. toctree::
   :maxdepth: 1

   architecture
   hyperparameter
   results
   freq_analysis