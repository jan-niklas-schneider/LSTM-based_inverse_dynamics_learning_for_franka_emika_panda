Network Architecture
====================

The image below shows the neural network (stellar-sweep-529) resulting from hyperparameter search used for torque estimation. 

.. image:: ../../src/Inverse_Dynamics/test_results/model_plot.png