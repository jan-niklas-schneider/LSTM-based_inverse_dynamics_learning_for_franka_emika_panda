Since the data set versions has different 
formats, two different analysis scripts are 
used. The analysis itself is identical.
Only the data reading functions are different. 


* - data_trajectory_visualizer
    - visualize one chosen trajectory
    of the data set v2. It can plot q, qd, qdd,
    tau and u, i, p over time and per axis.