#%%
# Import and variables
import sys
sys.path.append("..")
sys.path.append("src")
import numpy as np
import matplotlib.pyplot as plt
import roboticstoolbox as rtb
import rwth_style
import os

# Global variables
label_meas  = "Measurements"
label_rtb_1  = "RTB Estimation based on Measurements"
label_rtb_2  = "RTB Estimation based on Command"
label_com  = "Command by MoveIt"
label_derivative  = "Derivative based on q_command"
labels_axes = ['Axis 1', 'Axis 2', 'Axis 3', 'Axis 4', 'Axis 5', 'Axis 6', 'Axis 7']
trajectory_number = -1
directory_in = "/home/jaschneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/data/dataset_v1/train"
file_list = list(filter(lambda k: 'meas.csv' in k, sorted(os.listdir(directory_in))))
filename2 = file_list[trajectory_number]

# Load file and create variables
data2 = np.genfromtxt(os.path.join(directory_in, filename2), dtype=float, delimiter=',') 
t_meas = data2[:,0]
q_meas = data2[:,1:8]
qd_meas = data2[:,8:15]
tau_meas = data2[:,15:22]
try:
    u_meas = data2[:,22]
    i_meas = data2[:,23]
    p_meas = data2[:,24]
except: 
    print("No power information in database")

filename = filename2.replace("meas", "com")
data1 = np.genfromtxt(os.path.join(directory_in, filename), dtype=float, delimiter=',') 
t_command = data1[:,0]
q_command = data1[:,1:8]
qd_command = data1[:,8:15]
qdd_command = data1[:,15:22]
   
qd_command_diff = np.diff(q_command, axis=0)/np.diff(t_command)[:,None]
t_command_diff1 = (t_command[1:] + t_command[:-1]) / 2
qdd_command_diff = np.diff(qd_command_diff, axis=0)/np.diff(t_command_diff1)[:,None]
t_command_diff2 = (t_command_diff1[1:] + t_command_diff1[:-1]) / 2

tau_meas_interpolated = np.empty(q_command.shape)
for i in range(0, tau_meas.shape[1]):
    tau_meas_interpolated[:,i] = np.interp(t_command, t_meas, tau_meas[:,i])
tau_meas_interpolated = np.array(tau_meas_interpolated)

panda     = rtb.models.DH.Panda()
tau_rtb   = panda.rne(q=q_command, qd=qd_command, qdd=qdd_command, gravity=(0,0,-9.81), fext=None)

#%%
traj_com_rtb  = panda.fkine(q_command).t
traj_meas_rtb  = panda.fkine(q_meas).t
with plt.style.context("default"):
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    # ax.set_aspect('equal')
    X = traj_meas_rtb[:,0]
    Y = traj_meas_rtb[:,1]
    Z = traj_meas_rtb[:,2]
    ax.scatter(X, Y, Z, color=rwth_style.green, label=label_meas, s=2, marker='.')    
    X = traj_com_rtb[:,0]
    Y = traj_com_rtb[:,1]
    Z = traj_com_rtb[:,2]
    ax.scatter(X, Y, Z, color=rwth_style.blue, label=label_com, s=2, marker='.')
    ax.set_xlabel('X [m]')
    ax.set_ylabel('Y [m]')
    ax.set_zlabel('Z [m]')
    max_range = np.array([X.max()-X.min(), Y.max()-Y.min(), Z.max()-Z.min()]).max() / 2.0

    mid_x = (X.max()+X.min()) * 0.5
    mid_y = (Y.max()+Y.min()) * 0.5
    mid_z = (Z.max()+Z.min()) * 0.5
    ax.set_xlim(mid_x - max_range, mid_x + max_range)
    ax.set_ylim(mid_y - max_range, mid_y + max_range)
    ax.set_zlim(mid_z - max_range, mid_z + max_range)
    handles, labels = ax.get_legend_handles_labels()
    fig.legend()                      
    fig.tight_layout()


#%% 
# Plot Position
with plt.style.context("default"):
        fig, axs = plt.subplots(4,2, figsize=(10,10))
        for k in range(len(labels_axes)):
            axs[k//2, k%2].plot(t_command, q_command[:,k], ".-", markersize=2, label=label_com, color=rwth_style.green)
            axs[k//2, k%2].scatter(t_meas, q_meas[:,k], label=label_meas, s=2, marker='.', color=rwth_style.blue)
            axs[k//2, k%2].grid()
            axs[k//2, k%2].set_title(labels_axes[k])
            axs[k//2, k%2].set_ylabel("Position  [rad]")
            axs[k//2, k%2].set_xlabel("Time [s]")   
        fig.delaxes(axs[3,1])
        handles, labels = axs[0,0].get_legend_handles_labels()
        pos1 = axs[3,1].get_position()
        fig.legend(handles, labels, loc='lower left', bbox_to_anchor=(pos1.x0+0.004, pos1.y0+0.032))                      
        fig.tight_layout()
        # fig.savefig("/root/deep-learning-based-robot-dynamics-modelling-in-robot-based-laser-material-processing/thesis/resources/{}_10{}_{}.svg".format(filename[9:-3], indices[traj_num//2], space[traj_num%2]))
        # plt.close(fig)
 

#%% 
# Plot Velocity
with plt.style.context("default"):
        fig, axs = plt.subplots(4,2, figsize=(10,10))
        for k in range(len(labels_axes)):
            axs[k//2, k%2].plot(t_command, qd_command[:,k], ".-", markersize=5, label=label_com, color=rwth_style.green)
            axs[k//2, k%2].scatter(t_meas, qd_meas[:,k], label=label_meas, s=2, marker='.', color=rwth_style.blue)
            axs[k//2, k%2].grid()
            axs[k//2, k%2].set_title(labels_axes[k])
            axs[k//2, k%2].set_ylabel("Velocity  [rad/s]")
            axs[k//2, k%2].set_xlabel("Time [s]")   
        fig.delaxes(axs[3,1])
        handles, labels = axs[0,0].get_legend_handles_labels()
        pos1 = axs[3,1].get_position()
        fig.legend(handles, labels, loc='lower left', bbox_to_anchor=(pos1.x0+0.004, pos1.y0+0.032))                      
        fig.tight_layout()
        # fig.savefig("/root/deep-learning-based-robot-dynamics-modelling-in-robot-based-laser-material-processing/thesis/resources/{}_10{}_{}.svg".format(filename[9:-3], indices[traj_num//2], space[traj_num%2]))
        # plt.close(fig)
  

#%% 
# Plot Acceleration
with plt.style.context("default"):
        fig, axs = plt.subplots(4,2, figsize=(10,10))
        for k in range(len(labels_axes)):
            ax2 = axs[k//2, k%2].twinx()
            axs[k//2, k%2].plot(t_command, qdd_command[:,k], ".-", markersize=5, label=label_com, color=rwth_style.green)
            axs[k//2, k%2].grid()
            axs[k//2, k%2].set_title(labels_axes[k])
            axs[k//2, k%2].set_ylabel("Acceleration  [rad/s²]")
            axs[k//2, k%2].set_xlabel("Time [s]") 
            ax2.spines['right'].set_color(rwth_style.blue) 
            ax2.tick_params(axis='y', colors=rwth_style.blue)
            ax2.spines['left'].set_color(rwth_style.green)
            axs[k//2, k%2].tick_params(axis='y', colors=rwth_style.green)
        fig.delaxes(axs[3,1])
        handles, labels = axs[0,0].get_legend_handles_labels()
        handles2, labels2 = ax2.get_legend_handles_labels()
        labels = labels + labels2
        handles = handles + handles2
        pos1 = axs[3,1].get_position()
        fig.legend(handles, labels, loc='lower left', bbox_to_anchor=(pos1.x0+0.004, pos1.y0+0.032))                      
        fig.tight_layout()
        # fig.savefig("/root/deep-learning-based-robot-dynamics-modelling-in-robot-based-laser-material-processing/thesis/resources/{}_10{}_{}.svg".format(filename[9:-3], indices[traj_num//2], space[traj_num%2]))
        # plt.close(fig)
        

#%% Plot Torque
with plt.style.context("default"):
        fig, axs = plt.subplots(4,2, figsize=(10,10))
        for k in range(len(labels_axes)):
            axs[k//2, k%2].scatter(t_meas, tau_meas[:,k], label=label_meas, s=2, marker='o', color=rwth_style.blue)
            axs[k//2, k%2].scatter(t_command, tau_rtb[:,k], label=label_rtb_2, s=2, marker='.', color=rwth_style.blue_light)
            axs[k//2, k%2].grid()
            axs[k//2, k%2].set_title(labels_axes[k])
            axs[k//2, k%2].set_ylabel("Torque [Nm]")
            axs[k//2, k%2].set_xlabel("Time [s]")   
        fig.delaxes(axs[3,1])
        handles, labels = axs[0,0].get_legend_handles_labels()
        pos1 = axs[3,1].get_position()
        fig.legend(handles, labels, loc='lower left', bbox_to_anchor=(pos1.x0+0.004, pos1.y0+0.032))                      
        fig.tight_layout()
        # fig.savefig("/root/deep-learning-based-robot-dynamics-modelling-in-robot-based-laser-material-processing/thesis/resources/{}_10{}_{}.svg".format(filename[9:-3], indices[traj_num//2], space[traj_num%2]))
        # plt.close(fig)

#%% Plot Current, Voltage, Power
with plt.style.context("default"):
    plt.scatter(t_meas, i_meas[:], label="Current i", s=2, marker='o', color=rwth_style.red)
    plt.grid()
    plt.ylabel("Current [A]")
    plt.xlabel("Time [s]")   
    fig.legend()
    fig.tight_layout()
#%% Plot Current, Voltage, Power
with plt.style.context("default"):
    plt.scatter(t_meas, u_meas[:], label="Voltage u", s=2, marker='o', color=rwth_style.red)
    plt.grid()
    plt.ylabel("Voltage [V]")
    plt.xlabel("Time [s]")   
    fig.legend()
    fig.tight_layout()
#%% Plot Current, Voltage, Power
with plt.style.context("default"):
    plt.scatter(t_meas, p_meas[:], label="Power p", s=2, marker='o', color=rwth_style.red)
    plt.grid()
    plt.ylabel("Power [W]")
    plt.xlabel("Time [s]")   
    fig.legend()
    fig.tight_layout()
    