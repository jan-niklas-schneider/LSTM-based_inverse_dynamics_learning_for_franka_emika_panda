import numpy as np
# Physical joint limits 
q_lim_max_phys = np.array([2.7973,  1.6628, 2.7973,-0.1698, 2.7973, 3.6525, 2.7973]) + 0.1
q_lim_min_phys = np.array([-2.7973, -1.6628, -2.7973, -2.9718, -2.7973, 0.1175, -2.7973]) - 0.1
qd_lim_max_phys = np.array([2.175, 2.175, 2.175, 2.175, 2.61, 2.61, 2.61])
qd_lim_min_phys = -1*qd_lim_max_phys
qdd_lim_max_phys = np.array([15, 7.5, 10, 12.5, 15, 20, 20])
qdd_lim_min_phys = -1*qdd_lim_max_phys
tau_lim_max_phys = np.array([87, 87, 87, 87, 12, 12, 12])
tau_lim_min_phys = -1*tau_lim_max_phys

# Moveit limits
# /opt/ros/noetic/share/franka_description/robots/panda/joint_limits.yaml
# /opt/ros/noetic/share/panda_moveit_config/config/joint_limits.yaml
q_lim_max_moveit = q_lim_max_phys - 0.1
q_lim_min_moveit = q_lim_min_phys + 0.1
qd_lim_max_moveit = qd_lim_max_phys
qd_lim_min_moveit = qd_lim_min_phys
qdd_lim_max_moveit = np.array([3.75, 1.875, 2.5, 3.125, 3.75, 5, 5])
qdd_lim_min_moveit = -1*qdd_lim_max_moveit
tau_lim_max_moveit = np.array([87, 87, 87, 87, 12, 12, 12])
tau_lim_min_moveit = -1*tau_lim_max_phys