panda = importrobot("panda_eeux2393.urdf");
panda.Gravity = [0 0 -9.81];
panda.DataFormat = 'row';
panda.Bodies{1,1}.Mass = 0.629769;
panda.Bodies{1,1}.CenterOfMass = [-0.041018, -0.00014, 0.049974];
panda.Bodies{1,1}.Inertia = [0.00315, 0.00388, 0.004285, 8.2904E-07, 0.00015, 8.2299E-06];
panda.Bodies{1,2}.Mass = 4.970684;
panda.Bodies{1,2}.CenterOfMass = [0.003875, 0.002081, -0.04762];
panda.Bodies{1,2}.Inertia = [0.70337, 0.70661, 0.0091170, -0.00013900, 0.0067720, 0.019169];
panda.Bodies{1,3}.Mass = 0.646926;
panda.Bodies{1,3}.CenterOfMass = [-0.003141, -0.02872, 0.003495];
panda.Bodies{1,3}.Inertia = [0.0079620, 2.8110e-02, 2.5995e-02, -3.9250e-3, 1.0254e-02, 7.0400e-04];
panda.Bodies{1,4}.Mass = 3.228604;
panda.Bodies{1,4}.CenterOfMass = [2.7518e-02, 3.9252e-02, -6.6502e-02];
panda.Bodies{1,4}.Inertia = [3.7242e-02, 3.6155e-02, 1.0830e-02, -4.7610e-03, -1.1396e-02, -1.2805e-02];
panda.Bodies{1,5}.Mass = 3.587895;
panda.Bodies{1,5}.CenterOfMass = [-5.317e-02, 1.04419e-01, 2.7454e-02];
panda.Bodies{1,5}.Inertia = [2.5853e-02, 1.9552e-02, 2.8323e-02, 7.7960e-03, -1.3320e-03, 8.6410e-03];
panda.Bodies{1,6}.Mass = 1.225946;
panda.Bodies{1,6}.CenterOfMass = [-1.1953e-02, 4.1065e-02, -3.8437e-02];
panda.Bodies{1,6}.Inertia = [3.5549e-02, 2.9474e-02, 8.6270e-03, -2.1170e-03, -4.0370e-03, 2.2900e-04];
panda.Bodies{1,7}.Mass = 1.666555;
panda.Bodies{1,7}.CenterOfMass = [6.0149e-02, -1.4117e-02, -1.0517e-02];
panda.Bodies{1,7}.Inertia = [1.9640e-03, 4.3540e-03, 5.4330e-03, 1.0900e-04, -1.1580e-03, 3.4100e-04];
panda.Bodies{1,8}.Mass = 7.35522e-01;
panda.Bodies{1,8}.CenterOfMass = [1.0517e-02, -4.252e-03, 6.1597e-02];
panda.Bodies{1,8}.Inertia = [1.2516e-02, 1.0027e-02, 4.8150e-03, -4.2800e-04, -1.1960e-03, -7.4100e-04];

myFiles = dir(fullfile('.\test_joint', '*interp_com.csv')); 
for k = 1:length(myFiles)
    command_trajectory = readmatrix(myFiles(k).name);
    joint_torque = zeros([height(command_trajectory), 8]);
    for t = 1:height(command_trajectory)
        q_com = command_trajectory(t,2:8);
        qd_com = command_trajectory(t,9:15);
        qdd_com = command_trajectory(t,16:22);
        joint_torque(t,2:8) = inverseDynamics(panda, q_com, qd_com, qdd_com);
        joint_torque(t,1) = command_trajectory(t,1);
    end
    T = array2table(joint_torque);
    T.Properties.VariableNames(1:8) = {'t_com','tau1_rtbm','tau2_rtbm','tau3_rtbm','tau4_rtbm','tau5_rtbm','tau6_rtbm','tau7_rtbm'};
    filename = strrep(myFiles(k).name,'_com','_rtbm'); 
    writetable(T,fullfile('.\test_new', filename))
end
