import sys
sys.path.append("/root/deep-learning-based-robot-dynamics-modelling-in-robot-based-laser-material-processing/src")
import tqdm
import numpy as np
import os
import panda_limits


directory_in = "/root/deep-learning-based-robot-dynamics-modelling-in-robot-based-laser-material-processing/data/dataset_v3/test_2"

for filename in sorted(os.listdir(directory_in)):
    if (filename.endswith("com.csv") and "interp" not in filename):
        data1 = np.genfromtxt(os.path.join(directory_in, filename), dtype=float, delimiter=',') 
        t_command = data1[:,0]
        motion_params = data1[:,1:22]
        filename2 = filename.replace("com", "meas")
        data2 = np.genfromtxt(os.path.join(directory_in, filename2), dtype=float, delimiter=',') 
        t_meas = data2[:,0]
        max = 1
        min = -1
        # Normalization:   (X                -  X_min)     / (X_max  - X_min)            * (max - min) + min
        # Positions
        for i in range(0, 7):
            motion_params[:, i] = (motion_params[:, i] - panda_limits.q_lim_min_phys[i] )    / (panda_limits.q_lim_max_phys[i] - panda_limits.q_lim_min_phys[i])      * (max - min) + min
        # Velocities
        for i in range(7, 14):  
            motion_params[:, i] = (motion_params[:, i] - panda_limits.qd_lim_min_phys[i-7] )   / (panda_limits.qd_lim_max_phys[i-7] - panda_limits.qd_lim_min_phys[i-7])    * (max - min) + min
        # Accelerations
        for i in range(14, 21):
            motion_params[:, i] = (motion_params[:, i] - panda_limits.qdd_lim_min_phys[i-14] )  / (panda_limits.qdd_lim_max_phys[i-14] - panda_limits.qdd_lim_min_phys[i-14])  * (max - min) + min
        
        # Interpolate command values
        motion_params_interpolated = np.empty([t_meas.shape[0],21])
        for i in range(0, 21):
            motion_params_interpolated[:,i] = (np.interp(t_meas, t_command, motion_params[:,i]))
        meas = np.hstack([t_meas[:, None], motion_params_interpolated])
        file_output = directory_in + "/" + filename.replace("com", "interp_com")
        np.savetxt(
            file_output,
            meas,
            delimiter=",",
            header="t_com, q1_com, q2_com, q3_com, q4_com, q5_com, q6_com, q7_com, qd1_com, qd2_com, qd3_com, qd4_com, qd5_com, qd6_com, qd7_com, qdd1_com, qdd2_com, qdd3_com, qdd4_com, qdd5_com, qdd6_com, qdd7_com",
        )

