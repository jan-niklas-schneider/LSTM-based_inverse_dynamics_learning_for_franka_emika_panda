import os
import pandas as pd

for filename in os.listdir("/home/jaschneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/data/dataset_v1/train_data_command"):
    if 'csv' in filename:
        f = os.path.join("/home/jaschneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/data/dataset_v1/train_data_command", filename)
        data = pd.read_csv(f, names=["# t_com", "q1_com", "q2_com", "q3_com", "q4_com", "q5_com", "q6_com", "q7_com", 
                                    "qd1_com", "qd2_com", "qd3_com", "qd4_com", "qd5_com", "qd6_com", "qd7_com", 
                                    "qdd1_com", "qdd2_com", "qdd3_com", "qdd4_com", "qdd5_com", "qdd6_com", "qdd7_com"])
        data['# t_com'] = data['# t_com'].div(1000) 
        filename_out = filename.replace("control.csv", "com")
        data.to_csv('/home/jaschneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/data/dataset_v1/train/{}.csv'.format(filename_out), index=False)  

for filename in os.listdir("/home/jaschneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/data/dataset_v1/train_data_raw"):
    if 'csv' in filename:
        f = os.path.join("/home/jaschneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/data/dataset_v1/train_data_raw", filename)
        data = pd.read_csv(f, names=["# t_meas", "q1_meas", "q2_meas", "q3_meas", "q4_meas", "q5_meas", "q6_meas", "q7_meas", 
                                    "qd1_meas", "qd2_meas", "qd3_meas", "qd4_meas", "qd5_meas", "qd6_meas", "qd7_meas", 
                                    "tau1_meas", "tau2_meas", "tau3_meas", "tau4_meas", "tau5_meas", "tau6_meas", "tau7_meas"]) 
        data['# t_meas'] = data['# t_meas'] - data.iloc[0]['# t_meas']
        filename_out = filename.replace(".csv", "_meas")
        data.to_csv('/home/jaschneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/data/dataset_v1/train/{}.csv'.format(filename_out), index=False)  

for filename in os.listdir("/home/jaschneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/data/dataset_v1/test_data_command"):
    if 'csv' in filename:
        f = os.path.join("/home/jaschneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/data/dataset_v1/test_data_command", filename)
        data = pd.read_csv(f, names=["# t_com", "q1_com", "q2_com", "q3_com", "q4_com", "q5_com", "q6_com", "q7_com", 
                                    "qd1_com", "qd2_com", "qd3_com", "qd4_com", "qd5_com", "qd6_com", "qd7_com", 
                                    "qdd1_com", "qdd2_com", "qdd3_com", "qdd4_com", "qdd5_com", "qdd6_com", "qdd7_com"]) 
        data['# t_com'] = data['# t_com'].div(1000) 
        filename_out = filename.replace("control.csv", "com")
        data.to_csv('/home/jaschneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/data/dataset_v1/test/{}.csv'.format(filename_out), index=False)  

for filename in os.listdir("/home/jaschneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/data/dataset_v1/test_data_raw"):
    if 'csv' in filename:
        f = os.path.join("/home/jaschneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/data/dataset_v1/test_data_raw", filename)
        data = pd.read_csv(f, names=["# t_meas", "q1_meas", "q2_meas", "q3_meas", "q4_meas", "q5_meas", "q6_meas", "q7_meas", 
                                    "qd1_meas", "qd2_meas", "qd3_meas", "qd4_meas", "qd5_meas", "qd6_meas", "qd7_meas", 
                                    "tau1_meas", "tau2_meas", "tau3_meas", "tau4_meas", "tau5_meas", "tau6_meas", "tau7_meas"]) 
        data['# t_meas'] = data['# t_meas'] - data.iloc[0]['# t_meas']
        filename_out = filename.replace(".csv", "_meas")
        data.to_csv('/home/jaschneider/LSTM-based_inverse_dynamics_learning_for_franka_emika_panda/data/dataset_v1/test/{}.csv'.format(filename_out), index=False)                        