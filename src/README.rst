The src-directory is split into four subfolders:


.. code-block:: text

    src: here you find source code that is not directly related to data generation, preprocessing, analysis and neural network 
    │   ├── Data_Analysis: analysis scripts based on python
    │   ├── Data_Preprocessing: 
    │   ├── Inverse_Dynamics: Deep Learning source code
    │   │   ├── test results: figures and csv files 
    │   │   └── ... 
    │   ├── ROS: Data generation with ROS
    │   └── ...