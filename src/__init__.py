#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""Top-level package for Deep Learning-based robot dynamics modelling in robot-based laser material processing."""

__author__ = """Jan-Niklas Schneider"""
__email__ = 'jan-niklas.schneider@llt.rwth-aachen.de'
__version__ = '0.0.1'
