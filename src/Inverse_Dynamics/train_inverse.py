# %% How to Start a sweep: 
# 1. Init a new sweep:
#   python3 -m wandb sweep sweep.yaml
# 2. Start a new agent:
#   e.g.: python3 -m wandb agent --count $NUM jan-niklas_schneider/inverse_dynamic/vuhwux82 
# 3. If needed: Parallelize on a multi-GPU machine
#   CUDA_VISIBLE_DEVICES=0 wandb agent sweep_ID
#   CUDA_VISIBLE_DEVICES=1 wandb agent sweep_ID

import sys
sys.path.append("/root/deep-learning-based-robot-dynamics-modelling-in-robot-based-laser-material-processing/src")
import wandb
from argparse import ArgumentParser, Namespace
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout
from keras.losses import MeanSquaredError
from keras.callbacks import Callback
from keras import optimizers
from keras.utils.vis_utils import plot_model
from wandb.keras import WandbCallback
from rich import print
from panda_dynamics_shared import *
import tensorflow as tf
import tensorflow_text as tf_text

# %% Initialize LSTM model
def make_model(config:Namespace)->Sequential:
    """Neural network architecture is build here.
    
    Args:
        config (Namespace): Input configuration used for NN

    Returns:
        Sequential: The model made
    """
    model = Sequential()
    if (config.layers>1):
        for i in range(config.layers-1):
            model.add(LSTM(units=config.units, return_sequences=True, input_shape=(X_train.shape[1], X_train.shape[2]), 
                       activation = "tanh", recurrent_activation = "sigmoid", recurrent_dropout = 0, unroll = False, use_bias = True))  # input shape: (timesteps, features)
        model.add(LSTM(units=config.units, return_sequences=False))  # input shape: (timesteps, features)
    else:
        model.add(LSTM(units=config.units, return_sequences=False, input_shape=(X_train.shape[1], X_train.shape[2]), 
                       activation = "tanh", recurrent_activation = "sigmoid", recurrent_dropout = 0, unroll = False, use_bias = True))  # input shape: (timesteps, features)
    model.add(Dropout(config.dropout))
    model.add(Dense(units=Y_train.shape[1]))
    opt = optimizers.Adam(learning_rate=config.learning_rate, clipvalue=config.clipnorm)   # adding clip norm to work around exploding gradients, 1 yields not convergent loss, 100
    model.compile(optimizer=opt, loss=MeanSquaredError(), run_eagerly=True)
    model.build(X_train.shape)
    model.summary()
    # plot_model(model, to_file='model_output.png',
    #            show_shapes=True, show_layer_names=True)
    return model

class Metrics(Callback):
    def on_epoch_end(self, batch, logs={}):
        # y_pred = model(X_val, training=True)
        # score = backend.mean(metrics.mean_squared_error(Y_val, y_pred))
        # wandb.log({'val_loss_adjusted': score})
        return

# %% Train model with config values
def train(model:Sequential, config:Namespace) -> None:
    """Training function of the NN 

    Args:
        model (Sequential): Neural Network model
        config (Namespace): Configuration 
    """      
    with wandb.init() as run:
        config = wandb.config
        print("Training...")
        loss = model.fit(
                X_train,
                Y_train,
                epochs=config.epochs,
                shuffle=True,
                batch_size=config.batch_size,
                verbose=1,
                validation_data = (X_val, Y_val),
                callbacks=[
                    Metrics(), 
                    WandbCallback(
                        monitor="val_loss",
                        mode="min",
                        save_model=False,
                        validation_data=(X_val, Y_val)
                        )
                    ]
                )
    return

# %% Parse arguments to config
if __name__ == '__main__':
    parser = ArgumentParser()
    #trainer specific arguments
    parser.add_argument("--epochs", type=int, default=100)
    parser.add_argument("--learning_rate", type=float, default=0.1)
    # model specific arguments
    parser.add_argument("--batch_size", type=int, default=128)
    parser.add_argument("--dropout", type=float, default=0.1)
    parser.add_argument("--units", type=int, default=50)
    parser.add_argument("--window_size", type=int, default=10)
    parser.add_argument("--layers", type=int, default=1)
    # optimizer specific args
    parser.add_argument("--clipnorm", type=float, default=100)
    # Argparser
    args = parser.parse_args()
    vars(args)
    # w&b init
    wandb.init(config=args, project="Panda_Inverse_Dynamics")
    config = wandb.config
    
    directory_in = "/root/deep-learning-based-robot-dynamics-modelling-in-robot-based-laser-material-processing/data/dataset_v3/train"
    X_input = tf.convert_to_tensor(np.empty((0, 21)))
    Y_input = tf.convert_to_tensor(np.empty((0, 7)))
    for filename in tqdm(sorted(os.listdir(directory_in))):
        if filename.endswith("interp_com.csv"):
            data1 = np.genfromtxt(os.path.join(directory_in, filename), dtype=float, delimiter=',') 
            filename2 = filename.replace("interp_com", "meas")
            data2 = np.genfromtxt(os.path.join(directory_in, filename2), dtype=float, delimiter=',')
            X_input = tf.concat([X_input, tf.convert_to_tensor(data1[:,1:22])], 0)
            Y_input = tf.concat([Y_input, tf.convert_to_tensor(data2[:,15:22])], 0)

    print("X_input.shape: {}".format(X_input.shape))
    print("Y_input.shape: {}".format(Y_input.shape))
    # Split Dataset to train and validation
    print("Split Dataset to train and validation")
    train_size = int(0.8*X_input.shape[0])
    val_size = X_input.shape[0] - train_size
    print("train_size: {}".format(train_size))
    print("val_size: {}".format(val_size))
    X_train, X_val = tf.split(X_input, [train_size, val_size], axis=0)
    _, Y_train, _, Y_val = tf.split(Y_input, [config.window_size-1, train_size-config.window_size+1, config.window_size-1, val_size-config.window_size+1], 0)
    print("X_train.shape: {}".format(X_train.shape))
    print("X_val.shape: {}".format(X_val.shape))
    print("Y_train.shape: {}".format(Y_train.shape))
    print("Y_val.shape: {}".format(Y_val.shape))
    # SLiding Window
    print("Sliding Window")
    X_train = tf_text.sliding_window(data=X_train, width=config.window_size, axis=0)
    X_val = tf_text.sliding_window(data=X_val, width=config.window_size, axis=0)
    # Y_train = tf_text.sliding_window(data=Y_train, width=config.window_size, axis=0)
    # Y_val = tf_text.sliding_window(data=Y_val, width=config.window_size, axis=0)     
    print("X_train.shape: {}".format(X_train.shape))
    print("X_val.shape: {}".format(X_val.shape))
    print("Y_train.shape: {}".format(Y_train.shape))
    print("Y_val.shape: {}".format(Y_val.shape))
    
    # Model + Training
    model = make_model(config)
    train(model, config)
    
    print("Save model...")
    model.save('/root/deep-learning-based-robot-dynamics-modelling-in-robot-based-laser-material-processing/models/inverse_dynamics/stellar-sweep-529.h5')
    print("Model saved")
