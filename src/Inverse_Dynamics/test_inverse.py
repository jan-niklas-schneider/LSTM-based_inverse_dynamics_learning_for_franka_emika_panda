#%%
# Import
import sys
sys.path.append("src")
import numpy as np
import keras
import os
import matplotlib.pyplot as plt
import roboticstoolbox as rtb
import rwth_style
import pandas as pd
from rich import print
from rich.console import Console
from rich.table import Table
import tensorflow_text as tf_text
from rich.progress import track
import tensorflow as tf

#%% 
# Load Model and run test dataset
model_name = "stellar-sweep-529.h5"
model = keras.models.load_model("/root/deep-learning-based-robot-dynamics-modelling-in-robot-based-laser-material-processing/models/inverse_dynamics/{}".format(model_name)) 
window_size = model.input_shape[1]
keras.utils.vis_utils.plot_model(model, to_file='src/Inverse_Dynamics/test_results/model_plot.png', show_shapes=True, show_layer_names=True)

#%%
rms_joint_nn   = np.empty([0,7])
rms_joint_rtbm = np.empty([0,7])
rms_joint_rtbp = np.empty([0,7])

test_dataset = "data/dataset_v3/test"
for filename in track(sorted(os.listdir(test_dataset)), description='[green]Process of whole Test Dataset'):
    if filename.endswith("interp_com.csv"):
        # Command Traj
        data_com = np.genfromtxt(os.path.join(test_dataset, filename), dtype=float, delimiter=',') 
        t_command = data_com[:,0]
        q_command = data_com[:,1:8]
        qd_command = data_com[:,8:15]
        qdd_command = data_com[:,15:22]
        # Measurement Traj
        filename2 = filename.replace("interp_com", "meas")
        data_meas = np.genfromtxt(os.path.join(test_dataset, filename2), dtype=float, delimiter=',')
        t_meas = data_meas[:,0]
        q_meas = data_meas[:,1:8]
        qd_meas = data_meas[:,8:15]
        tau_meas = data_meas[:,15:22]
        # Load RTB-Matlab torques
        filename3 = filename.replace("interp_com", "interp_rtbm")
        data_rtbm = np.genfromtxt(os.path.join(test_dataset, filename3), dtype=float, delimiter=',')
        data_rtbm = data_rtbm[~np.isnan(data_rtbm).any(axis=1)]
        t_rtbm = data_rtbm[:,0]
        tau_rtbm = data_rtbm[:,1:8]
        # Calculate RTB-Python torques
        panda = rtb.models.DH.Panda()
        tau_rtbp = panda.rne(q=q_command, qd=qd_command, qdd=qdd_command, gravity=(0,0,-9.81))
        # Convert to tensors
        X = tf.convert_to_tensor(data_com[:,1:22])
        # Sliding Window
        X_test = tf_text.sliding_window(data=X, width=window_size, axis=0)
        y_pred = model.predict(X_test, verbose=1)
    
        rms_nn   = np.sqrt(((tau_meas[:-window_size+1] - y_pred)**2).mean(axis=0))
        rms_rtbp = np.sqrt(((tau_meas - tau_rtbp)**2).mean(axis=0))
        rms_rtbm = np.sqrt(((tau_meas - tau_rtbm)**2).mean(axis=0))
        
        if "ISO" not in filename:
            rms_joint_nn = np.vstack([rms_joint_nn, rms_nn])
            rms_joint_rtbp = np.vstack([rms_joint_rtbp, rms_rtbp])
            rms_joint_rtbm = np.vstack([rms_joint_rtbm, rms_rtbm])

        df = pd.DataFrame({"NN":rms_nn, "RTB-M":rms_rtbm, "RTB-P":rms_rtbp})
        df = df.append({"NN":rms_nn.mean(), "RTB-M":rms_rtbm.mean(), "RTB-P":rms_rtbp.mean()}, ignore_index=True)
        df.index = ['RMS Axis 1 [Nm]', 'RMS Axis 2 [Nm]', 'RMS Axis 3 [Nm]', 'RMS Axis 4 [Nm]', 'RMS Axis 5 [Nm]', 'RMS Axis 6 [Nm]', 'RMS Axis 7 [Nm]', 'RMS All [Nm]']
        df.T.to_csv('src/Inverse_Dynamics/test_results/test_result_{}.csv'.format(filename.replace("_interp_com.csv", "")), float_format='%.3f')
                
        # Print table
        # table = Table(title="Root Mean Square Errors")
        # table.add_column("Method")
        # table.add_column("Axis 1 [Nm]")
        # table.add_column("Axis 2 [Nm]")
        # table.add_column("Axis 3 [Nm]")
        # table.add_column("Axis 4 [Nm]")
        # table.add_column("Axis 4 [Nm]")
        # table.add_column("Axis 6 [Nm]")
        # table.add_column("Axis 7 [Nm]")
        # table.add_column("All [Nm]")
        # table.add_row("NN", "{}".format(rms_nn[0]), "{}".format(rms_nn[1]), "{}".format(rms_nn[2]), "{}".format(rms_nn[3]), "{}".format(rms_nn[4]), "{}".format(rms_nn[5]), "{}".format(rms_nn[6]), "{}".format(rms_nn.mean()))
        # table.add_row("RTB-M", "{}".format(rms_rtbm[0]), "{}".format(rms_rtbm[1]), "{}".format(rms_rtbm[2]), "{}".format(rms_rtbm[3]), "{}".format(rms_rtbm[4]), "{}".format(rms_rtbm[5]), "{}".format(rms_rtbm[6]), "{}".format(rms_rtbm.mean()))
        # table.add_row("RTB-P", "{}".format(rms_rtbp[0]), "{}".format(rms_rtbp[1]), "{}".format(rms_rtbp[2]), "{}".format(rms_rtbp[3]), "{}".format(rms_rtbp[4]), "{}".format(rms_rtbp[5]), "{}".format(rms_rtbp[6]), "{}".format(rms_rtbp.mean()))
        # console = Console()
        # console.print(table)
        
        # Plot test trajectory
        with plt.style.context("default"):
            plt.rcParams['savefig.dpi'] = 300
            num_row = 3
            num_col = 3
            fig, axs = plt.subplots(num_row, num_col, figsize=(num_col*6, num_row*3))
            for k in range(7):
                axs[k//num_col, k%num_col].axvspan(t_meas[0], t_meas[window_size], facecolor=rwth_style.blue, alpha=0.5, label="Initialization Phase for NN")
                axs[k//num_col, k%num_col].plot(t_meas[:], tau_meas[:,k], label="Measurement", color=rwth_style.green)
                axs[k//num_col, k%num_col].scatter(t_meas[window_size-1:], y_pred[:,k], label="Prediction based on command trajectory (NN)", marker='o', s=2, color=rwth_style.blue)
                axs[k//num_col, k%num_col].scatter(t_meas[:], tau_rtbp[:,k],  label="Estimation based on command trajectory (RTB-P)", marker='o', s=2, color=rwth_style.blue_light)
                axs[k//num_col, k%num_col].scatter(t_rtbm[:], tau_rtbm[:,k],  label="Estimation based on command trajectory (RTB-M)", marker='o', s=2, color=rwth_style.orange)
                axs[k//num_col, k%num_col].set_title('Axis {}'.format(k+1))
                axs[k//num_col, k%num_col].set_ylabel("Torque [Nm]")
                axs[k//num_col, k%num_col].set_xlabel("Time [s]")   
                if (k < 4):
                    axs[k//num_col, k%num_col].set_ylim([-90, 90]) 
                    axs[k//num_col, k%num_col].set_yticks([-87, -40, 0, 40, 87]) 
                else:
                    axs[k//num_col, k%num_col].set_ylim([-12, 12]) 
                    axs[k//num_col, k%num_col].set_yticks([-12, -6, 0, 6, 12]) 
                axs[k//num_col, k%num_col].grid(which='both')
                axs[k//num_col, k%num_col].set_xlim([0, t_meas.max()])
            # Detail Axis
            axs[2,-2].axvspan(t_meas[0], t_meas[window_size], facecolor=rwth_style.blue, alpha=0.5, label="Initialization Phase for NN")
            axs[2,-2].plot(t_meas[:], tau_meas[:,1], label="Measurement", color=rwth_style.green)
            axs[2,-2].scatter(t_meas[window_size-1:], y_pred[:,1], label="Prediction based on command trajectory (NN)", marker='o', s=2, color=rwth_style.blue)
            axs[2,-2].scatter(t_meas[:], tau_rtbp[:,1],  label="Estimation based on command trajectory (RTB-P)", marker='o', s=2, color=rwth_style.blue_light)
            axs[2,-2].scatter(t_rtbm[:], tau_rtbm[:,1],  label="Estimation based on command trajectory (RTB-M)", marker='o', s=2, color=rwth_style.orange)
            axs[2,-2].grid()
            axs[2,-2].set_title('Detailed View of Axis 2')
            axs[2,-2].set_ylabel("Torque [Nm]")
            axs[2,-2].set_xlabel("Time [s]")   
            axs[2,-2].set_xlim([0, t_meas.max()])  
            # Allgemein
            fig.delaxes(axs[-1,-1])
            handles, labels = axs[0,0].get_legend_handles_labels()
            pos1 = axs[-1,-1].get_position()
            fig.legend(handles, labels, loc='lower left', bbox_to_anchor=(pos1.x0+0.02, pos1.y0-0.055))                      
            fig.tight_layout()
            fig.savefig("src/Inverse_Dynamics/test_results/test_result_{}.png".format(filename.replace("_interp_com.csv", "")))
            plt.close(fig)
                
       
        with plt.style.context("default"):
            plt.rcParams['savefig.dpi'] = 300
            num_row = 4
            num_col = 2
            fig, axs = plt.subplots(num_row, num_col, figsize=(num_col*6, num_row*3))
            for k in range(7):
                n = t_meas.size
                freq = np.fft.fftfreq(n = n, d = 1.0 / 100)
                axs[k//num_col, k%num_col].plot(freq[:n//2], 2.0/n * np.abs(np.fft.fft(tau_meas[:,k])[:n//2]), label="Measurement", color=rwth_style.green)
                axs[k//num_col, k%num_col].plot(freq[:n//2], 2.0/n * np.abs(np.fft.fft(tau_rtbp[:,k])[:n//2]), label="Estimation based on command trajectory (RTB-P)", color=rwth_style.blue_light)
                axs[k//num_col, k%num_col].plot(freq[:n//2], 2.0/n * np.abs(np.fft.fft(tau_rtbm[:,k])[:n//2]), label="Estimation based on command trajectory (RTB-M)", color=rwth_style.orange)
                n = t_meas.size-window_size+1
                freq = np.fft.fftfreq(n = n, d = 1.0 / 100)
                axs[k//num_col, k%num_col].plot(freq[:n//2], 2.0/n * np.abs(np.fft.fft(y_pred[:,k])[:n//2]), label="Prediction based on command trajectory (NN)", color=rwth_style.blue)
                axs[k//num_col, k%num_col].set_title('Axis {}'.format(k+1))
                axs[k//num_col, k%num_col].set_ylabel("Magnitude")
                axs[k//num_col, k%num_col].set_xlabel("Frequency [Hz]") 
                axs[k//num_col, k%num_col].set_xlim([0,50])
                axs[k//num_col, k%num_col].set_yscale('log')
                axs[k//num_col, k%num_col].grid()
            fig.delaxes(axs[-1,-1])
            handles, labels = axs[0,0].get_legend_handles_labels()
            pos1 = axs[-1,1].get_position()
            fig.legend(handles, labels, loc='lower left', bbox_to_anchor=(pos1.x0, pos1.y0))                      
            fig.tight_layout()
            fig.savefig("src/Inverse_Dynamics/test_results/test_result_fft_{}.png".format(filename.replace("_interp_com.csv", "")))
            plt.close(fig)
        
        if "20230215_114651" in filename:
            with plt.style.context("default"):
                plt.rcParams['savefig.dpi'] = 300
                fig, axs = plt.subplots(2, 1, figsize=(1*6, 2*3))
                k=2
                n = t_meas.size
                freq = np.fft.fftfreq(n = n, d = 1.0 / 100)
                axs[0].plot(freq[:n//2], 2.0/n * np.abs(np.fft.fft(tau_meas[:,k])[:n//2]), label="Measurement", color=rwth_style.green)
                axs[0].plot(freq[:n//2], 2.0/n * np.abs(np.fft.fft(tau_rtbp[:,k])[:n//2]), label="Estimation based on command trajectory (RTB-P)", color=rwth_style.blue_light)
                axs[0].plot(freq[:n//2], 2.0/n * np.abs(np.fft.fft(tau_rtbm[:,k])[:n//2]), label="Estimation based on command trajectory (RTB-M)", color=rwth_style.orange)
                n = t_meas.size-window_size+1
                freq = np.fft.fftfreq(n = n, d = 1.0 / 100)
                axs[0].plot(freq[:n//2], 2.0/n * np.abs(np.fft.fft(y_pred[:,k])[:n//2]), label="Prediction based on command trajectory (NN)", color=rwth_style.blue)
                axs[0].set_title('Axis {}'.format(k+1))
                axs[0].set_ylabel("Magnitude")
                axs[0].set_xlabel("Frequency [Hz]") 
                axs[0].set_xlim([0,50])
                axs[0].set_yscale('log')
                axs[0].grid()
                fig.delaxes(axs[-1])
                pos1 = axs[-1].get_position()
                fig.legend(handles, labels, loc='lower center', bbox_to_anchor=(0.5, pos1.y0+0.18)) 
                # plt.tight_layout()
                plt.savefig("src/Inverse_Dynamics/test_results/test_result_fft_{}_axis3.png".format(filename.replace("_interp_com.csv", "")))
                plt.close(fig)
            
            
df = pd.DataFrame({"NN":rms_joint_nn.mean(axis = 0), "RTB-M":rms_joint_rtbm.mean(axis = 0), "RTB-P":rms_joint_rtbp.mean(axis = 0)})
df = df.append({"NN":rms_joint_nn.mean(), "RTB-M":rms_joint_rtbm.mean(), "RTB-P":rms_joint_rtbp.mean()}, ignore_index=True)
df.index = ['RMS Axis 1 [Nm]', 'RMS Axis 2 [Nm]', 'RMS Axis 3 [Nm]', 'RMS Axis 4 [Nm]', 'RMS Axis 5 [Nm]', 'RMS Axis 6 [Nm]', 'RMS Axis 7 [Nm]', 'RMS All [Nm]']
df.T.to_csv('src/Inverse_Dynamics/test_results/test_result_average_joint.csv', float_format='%.3f')
        