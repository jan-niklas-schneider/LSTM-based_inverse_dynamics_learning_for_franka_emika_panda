Inverse Dynamic Modelling
#########################

Overview
********

This is the approach to model the Franka 
Emika Panda robot's inverse dynamic with 
deep learning approach.

.. list-table:: 
   :header-rows: 1

   * - Script 
     - Discription
   * - sweep.yaml
     - contains sweep parameters for w&b
   * - test_inverse
     - Contains the neural network test and 
        can plot the predicted output as well 
        as the robotic toolbox calculations 
        (pyhton and external matlab csv)
   * - train_inverse
     - contains the training script. Builds 
        neural network based on sweep.yaml config